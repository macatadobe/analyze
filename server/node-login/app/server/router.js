
var CT = require('./modules/country-list');
var AM = require('./modules/account-manager');
var EM = require('./modules/email-dispatcher');



var http = require("http");
var url = require("url");
var multipart = require("multipart");
var sys = require("sys");
//var fs = require("fs");
var fs = require("fs-extra");
var formidable = require('formidable');
var util = require('util');

module.exports = function(app) {

	app.post('/upload', function(req, res){
                	var form = new formidable.IncomingForm();
    			form.parse(req, function(err, fields, files) {
			console.log("[200] " + req.method + " to " + req.url);
      			res.writeHead(200, {'content-type': 'text/plain'});
      			res.write('received upload:\n\n');
      			res.end(util.inspect({fields: fields, files: files}));
    	           });
    		    //return; 
        });  
	// main login page //

	app.get('/', function(req, res){
			// check if the user's credentials are saved in a cookie //
			if (req.cookies.user == undefined || req.cookies.pass == undefined){
			res.render('login', { title: 'Hello - Please Login To Your Account' });
			}	else{
			// attempt automatic login //
			AM.autoLogin(req.cookies.user, req.cookies.pass, function(o){
				if (o != null){
				req.session.user = o;
				res.redirect('/home');
				}	else{
				res.render('login', { title: 'Hello - Please Login To Your Account' });
				}
				});
			}
			});

	app.post('/', function(req, res){
			AM.manualLogin(req.param('user'), req.param('pass'), function(e, o){
				if (!o){
				res.send(e, 400);
				}	else{
				req.session.user = o;
				if (req.param('remember-me') == 'true'){
				res.cookie('user', o.user, { maxAge: 900000 });
				res.cookie('pass', o.pass, { maxAge: 900000 });
				}
				res.send(o, 200);
				}
				});
			});

	// logged-in user homepage //

	app.get('/home', function(req, res) {
			if (req.session.user == null){
			// if user is not logged-in redirect back to login page //
			res.redirect('/');
			}   else{
			res.render('home', {
title : 'Control Panel',
countries : CT,
udata : req.session.user
});
			}
			});

app.post('/home', function(req, res){
		if (req.param('user') != undefined) {
		AM.updateAccount({
user 		: req.param('user'),
name 		: req.param('name'),
email 		: req.param('email'),
country 	: req.param('country'),
pass		: req.param('pass')
}, function(e, o){
if (e){
res.send('error-updating-account', 400);
}	else{
req.session.user = o;
// update the user's login cookies if they exists //
if (req.cookies.user != undefined && req.cookies.pass != undefined){
res.cookie('user', o.user, { maxAge: 900000 });
res.cookie('pass', o.pass, { maxAge: 900000 });	
}
res.send('ok', 200);
}
});
}	else if (req.param('logout') == 'true'){
	res.clearCookie('user');
	res.clearCookie('pass');
	req.session.destroy(function(e){ res.send('ok', 200); });
}
});

// creating new accounts //

app.get('/signup', function(req, res) {
		res.render('signup', {  title: 'Signup', countries : CT });
		});

app.post('/signup', function(req, res){
		AM.addNewAccount({
name 	: req.param('name'),
email 	: req.param('email'),
user 	: req.param('user'),
pass	: req.param('pass'),
country : req.param('country')
}, function(e){
if (e){
res.send(e, 400);
}	else{
res.send('ok', 200);
}
});
		});

// password reset //

app.post('/lost-password', function(req, res){
		// look up the user's account via their email //
		AM.getAccountByEmail(req.param('email'), function(o){
			if (o){
			res.send('ok', 200);
			EM.dispatchResetPasswordLink(o, function(e, m){
				// this callback takes a moment to return //
				// should add an ajax loader to give user feedback //
				if (!e) {
				//	res.send('ok', 200);
				}	else{
				res.send('email-server-error', 400);
				for (k in e) console.log('error : ', k, e[k]);
				}
				});
			}	else{
			res.send('email-not-found', 400);
			}
			});
		});

app.get('/reset-password', function(req, res) {
		var email = req.query["e"];
		var passH = req.query["p"];
		AM.validateResetLink(email, passH, function(e){
			if (e != 'ok'){
			res.redirect('/');
			} else{
			// save the user's email in a session instead of sending to the client //
			req.session.reset = { email:email, passHash:passH };
			res.render('reset', { title : 'Reset Password' });
			}
			})
		});

app.post('/reset-password', function(req, res) {
		var nPass = req.param('pass');
		// retrieve the user's email from the session to lookup their account and reset password //
		var email = req.session.reset.email;
		// destory the session immediately after retrieving the stored email //
		req.session.destroy();
		AM.updatePassword(email, nPass, function(e, o){
			if (o){
			res.send('ok', 200);
			}	else{
			res.send('unable to update password', 400);
			}
			})
		});

// view & delete accounts //

app.get('/print', function(req, res) {
		AM.getAllRecords( function(e, accounts){
			res.render('print', { title : 'Account List', accts : accounts });
			})
		});

app.post('/delete', function(req, res){
		AM.deleteAccount(req.body.id, function(e, obj){
			if (!e){
			res.clearCookie('user');
			res.clearCookie('pass');
			req.session.destroy(function(e){ res.send('ok', 200); });
			}	else{
			res.send('record not found', 400);
			}
			});
		});

app.get('/reset', function(req, res) {
		AM.delAllRecords(function(){
			res.redirect('/print');	
			});
		});

		// manish code starts 
		app.get('/helloWorld', function(req,res) {
                        newFunc2(req,res);
			//res.send("Hello world . We hope you would like our app . This was our  first line of code we wrote in server :) ");
		}); 


                
		app.post('/SavePicture', function(req, res){
			 /* Display the file upload form. */
			res.writeHead(200, {'content-type': 'text/html'});
			res.end(
   				 '<form action="/upload" enctype="multipart/form-data" method="post">'+
 				   '<input type="text" name="title"><br>'+
				    '<input type="file" name="upload" multiple="multiple"><br>'+
				    '<input type="submit" value="upload">'+
				    '</form>'
			  );
		});

		app.post('/upload22', function(req, res){
                    var form = new formidable.IncomingForm();
    		    form.parse(req, function(err, fields, files) {
				console.log("[200] " + req.method + " to " + req.url);
      				res.writeHead(200, {'content-type': 'text/plain'});
      				res.write('received upload:\n\n');
      				res.end(util.inspect({fields: fields, files: files}));
    	            });
    		    //return; 
                });  

                app.post('/SavePicture222', function(req, res){
                        //upload_file(req, res);

			console.log("[200] " + req.method + " to " + req.url);
			req.on('data', function(chunk) {
              			console.log("Received body data:");
            		});
            		var form = new formidable.IncomingForm();
            		form.parse(req, function(err,fields, files) {
                		console.log('in if condition'+sys.inspect({fields: fields, files: files}));
                		console.log('in if condition'+sys.inspect({name: req.files.upload.name}));
               			fs.writeFile(files.upload.name, files.upload,'utf8', function (err) {
                      			if (err) throw err;
                      			console.log('It\'s saved!');
                			});

             		res.writeHead(200, {'content-type': 'text/plain'});
              		res.write('received upload:\n\n');
              		res.end();
            		});
            		req.on('end', function() {
              			// empty 200 OK response for now
              			res.writeHead(200, "OK", {'Content-Type': 'text/html'});
              			res.end();
            		});
		});

		app.post('/SavePictureOLD', function(req, res){
                        upload_file(req, res);
		});
                

               
		app.get('*', function(req, res) { res.render('404', { title: 'Page Not Found'}); });

	};


	function newFunc2(req,res) {
       		res.send("This is my new helper function");
        }

	/*
	 * Display upload form
	 */
	function display_form(req, res) {
	    res.sendHeader(200, {"Content-Type": "text/html"});
	    res.write(
        	  '<form action="/upload" method="post" enctype="multipart/form-data">'+
       		  	'<input type="file" name="upload-file">'+
      		  	'<input type="submit" value="Upload">'+
      		  '</form>'
    	    );
   	    res.close();
	}

	/*
	 * Create multipart parser to parse given request
	 */
	function parse_multipart(req) {
   		 var parser = multipart.parser();
		 // Make parser use parsed request headers
		 parser.headers = req.headers;

	          // Add listeners to request, transfering data to parser
  		  req.addListener("data", function(chunk) {
    		  	  parser.write(chunk);
    		  });

    		  req.addListener("end", function() {
        		  parser.close();
   		  });

    		  return parser;
	}


	/*
 	* Handle file upload
	 */
	function upload_file(req, res) {
    		// Request body is binary
		req.setEncoding("binary");
                 // manish .. https://github.com/felixge/node-couchdb/issues/9 says setBodyEnconding is now depricated
		//req.setBodyEncoding("binary");

	        // Handle request as multipart
		var stream = parse_multipart(req);

		var fileName = null;
                var fileStream = null;

		// Set handler for a request part received
	        stream.onPartBegin = function(part) {
                	sys.debug("Started part, name = " + part.name + ", filename = " + part.filename);

                	// Construct file name
                	fileName = "./uploads/" + stream.part.filename;

                	// Construct stream used to write to file
                	fileStream = fs.createWriteStream(fileName);

                	// Add error handler
                	fileStream.addListener("error", function(err) {
                		sys.debug("Got error while writing to file '" + fileName + "': ", err);
       	        	});

       			// Add drain (all queued data written) handler to resume receiving request data
        		fileStream.addListener("drain", function() {
           		 	req.resume();
        		});
    		};

    		// Set handler for a request part body chunk received
    		stream.onData = function(chunk) {
        		// Pause receiving request data (until current chunk is written)
        		req.pause();

        		// Write chunk to file
        		// Note that it is important to write in binary mode
        		// Otherwise UTF-8 characters are interpreted
        		sys.debug("Writing chunk");
        		fileStream.write(chunk, "binary");
    		};

    		// Set handler for request completed
    		stream.onEnd = function() {
        		// As this is after request completed, all writes should have been queued by now
        		// So following callback will be executed after all the data is written out
        		fileStream.addListener("drain", function() {
            			// Close file stream
            			fileStream.end();
            			// Handle request completion, as all chunks were already written
            			upload_complete(res);
        		});
    		};
	}	

	function upload_complete(res) {
    		sys.debug("Request complete");

   		 // Render response
    		res.sendHeader(200, {"Content-Type": "text/plain"});
    		res.write("Thanks for playing!");
    		res.end();

    		sys.puts("\n=> Done");
	}

	/*
 	* Handles page not found error
 	*/
	function show_404(req, res) {
    		res.sendHeader(404, {"Content-Type": "text/plain"});
    		res.write("You r doing it rong!");
    		res.end();
	}



